//
//  AppDelegate.swift
//  demo prebid
//
//  Created by Jan Sebastian on 22/06/23.
//

import UIKit
import GoogleMobileAds
import PrebidMobile
//import PrebidMobileGAMEventHandlers
//import PrebidMobileAdMobAdapters

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window:UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
//        Prebid.shared.prebidServerAccountId = "any_1.82_1.82"
        
        Prebid.shared.prebidServerAccountId = "Tr4424EtwA"
        
        do {
            try Prebid.shared.setCustomPrebidServer(url: "https://hb-rc.jixie.io/openrtb2/auction")
        } catch let error {
            print("prebid error: \(error)")
        }
        
        Prebid.initializeSDK(GADMobileAds.sharedInstance()) { status, error in
            if let error = error {
                print("Prebid Initialization Error: \(error.localizedDescription)")
            }
        }
        
        Targeting.shared.sourceapp = "6448592070"
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        #if DEBUG
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [GADSimulatorID, "4d6d741545cd2c87574a5ff6e6455f08", "02bc682baf973099a3ac6a190269244d", "64650238dec513236bd2d198ceaa47a6", "ac4a299d122b04ac0f2f812b3e2a4ff8", "3f17a2c588c2875e293bc597a978ce59"] //MARK: PLEASE CHECK THE CONSOLE FOR THIS VALUE
        #endif
//        AdMobUtils.initializeGAD()
//        GAMUtils.shared.initializeGAM()
        
        
        let vc = NewHomeViewController()
        let navBar = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = navBar
        self.window?.makeKeyAndVisible()
        
        return true
    }
}

