//
//  NewHomeViewController.swift
//  demo prebid
//
//  Created by Jan Sebastian on 24/07/23.
//

import UIKit
import AppTrackingTransparency
import RxSwift
import RxCocoa

class NewHomeViewController: UIViewController {
    
    private let tblContent: UITableView = {
        let tabel = UITableView()
        tabel.backgroundColor = UIColor(white: 1, alpha: 0.001)
        tabel.estimatedRowHeight = 60
//        tabel.register(RadioButtonTableViewCell.self, forCellReuseIdentifier: "cellSelectDaerah")
        tabel.register(AdsTableViewCell.self, forCellReuseIdentifier: "cellAds")
        tabel.register(NewsTableViewCell.self, forCellReuseIdentifier: "cellNews")
        tabel.separatorStyle = .none
        return tabel
    }()
    
    private var listNews: [ContentModel] = []
    private var disposedBag: DisposeBag = DisposeBag()
    private var streamFetch: PublishSubject<Void> = PublishSubject<Void>()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        setupConstraints()
        tblContent.delegate = self
        tblContent.dataSource = self
        listNews += [
            ContentModel(type: .Ads),
            ContentModel(type: .News),
            ContentModel(type: .News),
            ContentModel(type: .News),
            ContentModel(type: .News),
            ContentModel(type: .News),
            ContentModel(type: .News),
            ContentModel(type: .News),
            ContentModel(type: .News),
            ContentModel(type: .News),
            ContentModel(type: .News)
        ]
        tblContent.reloadData()
        
        streamFetch
            .debounce(.milliseconds(500),
                             scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] isNewPage in
                
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.listNews += [
                    ContentModel(type: .Ads),
                    ContentModel(type: .News),
                    ContentModel(type: .News),
                    ContentModel(type: .News),
                    ContentModel(type: .News),
                    ContentModel(type: .News),
                    ContentModel(type: .News),
                    ContentModel(type: .News),
                    ContentModel(type: .News),
                    ContentModel(type: .News),
                    ContentModel(type: .News)
                ]
                strongSelf.tblContent.reloadData()
            
            }).disposed(by: disposedBag)
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    private func setupView() {
        self.view.addSubview(tblContent)
    }
    
    private func setupConstraints() {
        let views: [String: Any] = ["tblContent": tblContent]
        
        var constraints: [NSLayoutConstraint] = []
        
        tblContent.translatesAutoresizingMaskIntoConstraints = false
        let v_tblContent = "V:|-[tblContent]-|"
        let h_tblContent = "H:|-1-[tblContent]-1-|"
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: v_tblContent, options: .alignAllLeading, metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: h_tblContent, options: .alignAllTop, metrics: nil, views: views)
        NSLayoutConstraint.activate(constraints)
    }
    
}

extension NewHomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if Int(scrollView.contentOffset.y) >= Int(scrollView.contentSize.height - scrollView.frame.size.height) {
            streamFetch.onNext(())
        }
    }
}

extension NewHomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let getData = listNews[indexPath.row]
        
        if getData.type == .Ads {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellAds", for: indexPath) as? AdsTableViewCell else {
                return UITableViewCell()
            }
            cell.setupValue(root: self)
            return cell
        } else if getData.type == .News {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellNews", for: indexPath) as? NewsTableViewCell else {
                return UITableViewCell()
            }
            return cell
        } else {
            return UITableViewCell()
        }
    }
}
