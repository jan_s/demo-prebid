//
//  NewsTableViewCell.swift
//  demo prebid
//
//  Created by Jan Sebastian on 24/07/23.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    private let lblTitle: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(white: 1, alpha: 0.001)
//        label.textColor = .black
        label.font = label.font.withSize(25)
        label.text = "this is news content sample"
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    
    private let viewBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 1, alpha: 0.001)
        return view
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraint()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    private func setupView() {
        self.contentView.addSubview(viewBackground)
        viewBackground.addSubview(lblTitle)
    }
    
    private func setupConstraint() {
        let views: [String: Any] = ["viewBackground": viewBackground]
        var constraints: [NSLayoutConstraint] = []
        
        viewBackground.translatesAutoresizingMaskIntoConstraints = false
        let v_viewBackground = "V:|-[viewBackground]-|"
        let h_viewBackground = "H:|-5-[viewBackground]-5-|"
        constraints += NSLayoutConstraint.constraints(withVisualFormat: v_viewBackground, options: .alignAllLeading, metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: h_viewBackground, options: .alignAllTop, metrics: nil, views: views)
        constraints += [NSLayoutConstraint(item: viewBackground, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)]
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        constraints += [NSLayoutConstraint(item: lblTitle, attribute: .centerX, relatedBy: .equal, toItem: viewBackground, attribute: .centerX, multiplier: 1, constant: 0)]
        constraints += [NSLayoutConstraint(item: lblTitle, attribute: .centerY, relatedBy: .equal, toItem: viewBackground, attribute: .centerY, multiplier: 1, constant: 0)]
        
        NSLayoutConstraint.activate(constraints)
    }
    
}
