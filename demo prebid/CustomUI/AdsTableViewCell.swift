//
//  AdsTableViewCell.swift
//  demo prebid
//
//  Created by Jan Sebastian on 24/07/23.
//

import UIKit
import PrebidMobile
import GoogleMobileAds

class AdsTableViewCell: UITableViewCell {

    private let emptyView: GADBannerView = {
        let view = GADBannerView(adSize: GADAdSizeLargeBanner)
        view.backgroundColor = UIColor(white: 1, alpha: 0.001)
        return view
    }()
    
    private let viewBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 1, alpha: 0.001)
        return view
    }()
    
    private var adUnit: BannerAdUnit = BannerAdUnit(configId: "1000442-09SuBz4Fpu", size: CGSize(width: 300, height: 250))
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraint()
        
        adUnit.setAutoRefreshMillis(time: 30000)
        let parameters = BannerParameters()
        parameters.api = [Signals.Api.MRAID_2]
        adUnit.bannerParameters = parameters
        
        emptyView.adUnitID = "/31800665/TribunnewsApp/prebidtesting"
        emptyView.delegate = self
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    private func setupView() {
//        self.backgroundColor = .white
//        self.contentView.backgroundColor = .white
        self.contentView.addSubview(viewBackground)
        viewBackground.addSubview(emptyView)
    }
    private func setupConstraint() {
        let views: [String: Any] = ["emptyView": emptyView,
                                    "viewBackground": viewBackground]
        var constraints: [NSLayoutConstraint] = []
        
        viewBackground.translatesAutoresizingMaskIntoConstraints = false
        let v_viewBackground = "V:|-[viewBackground]-|"
        let h_viewBackground = "H:|-5-[viewBackground]-5-|"
        constraints += NSLayoutConstraint.constraints(withVisualFormat: v_viewBackground, options: .alignAllLeading, metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: h_viewBackground, options: .alignAllTop, metrics: nil, views: views)
        constraints += [NSLayoutConstraint(item: viewBackground, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 300)]
        
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        let h_emptyView = "H:|-5-[emptyView]-5-|"
        let v_emptyView = "V:|-5-[emptyView]-5-|"
        constraints += NSLayoutConstraint.constraints(withVisualFormat: v_emptyView, options: .alignAllLeading, metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: h_emptyView, options: .alignAllTop, metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
}

extension AdsTableViewCell {
    func setupValue(root: UIViewController) {
        emptyView.rootViewController = root
        let gadRequest = GADRequest()
        adUnit.fetchDemand(adObject: gadRequest, completion: { [weak self] result in
            print(("Prebid demand fetch \(result.name())"))
            
            DispatchQueue.main.async {
                self?.emptyView.load(gadRequest)
            }
        })
    }
}

extension AdsTableViewCell: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewDidReceiveAd")
        
    }
    
    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        print("GADMobileAds bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func bannerViewDidRecordImpression(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewDidRecordImpression")
    }

    func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewWillPresentScreen")
    }

    func bannerViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewWillDIsmissScreen")
    }

  
    func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewDidDismissScreen")
    }
}
