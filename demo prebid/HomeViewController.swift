//
//  HomeViewController.swift
//  demo prebid
//
//  Created by Jan Sebastian on 22/06/23.
//

import UIKit
import AppTrackingTransparency
import PrebidMobile
import GoogleMobileAds

class HomeViewController: UIViewController {
    
    private let emptyView: GADBannerView = {
        let view = GADBannerView(adSize: GADAdSizeLargeBanner)
        view.backgroundColor = UIColor(white: 1, alpha: 0.001)
        return view
    }()
    
    private var adUnit: BannerAdUnit = BannerAdUnit(configId: "1000442-09SuBz4Fpu", size: CGSize(width: 300, height: 250))

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            if #available(iOS 14, *) {
                ATTrackingManager.requestTrackingAuthorization { status in
                    switch status {
                    case .authorized:
                        print("enable tracking")
                    case .denied:
                        print("disable tracking")
                    case .notDetermined:
                        print("disable tracking, waiting for answer")
                    default:
                        print("disable tracking")
                    }
                }
            } else {
                // Fallback on earlier versions
            }
        })
        setupView()
        setupConstraints()
        
        adUnit.setAutoRefreshMillis(time: 30000)
        let parameters = BannerParameters()
        parameters.api = [Signals.Api.MRAID_2]
        adUnit.bannerParameters = parameters
        
        emptyView.adUnitID = "/31800665/TribunnewsApp/prebidtesting"
        emptyView.delegate = self
        emptyView.rootViewController = self
        
        let gadRequest = GADRequest()
        adUnit.fetchDemand(adObject: gadRequest, completion: { [weak self] result in
            print(("Prebid demand fetch for AdMob \(result.name())"))
            
            // 6. Load ad
            DispatchQueue.main.async {
                self?.emptyView.load(gadRequest)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func setupView() {
        self.view.backgroundColor = .white
        self.view.addSubview(emptyView)
    }
    
    private func setupConstraints() {
        let views: [String: Any] = ["emptyView": emptyView]
        
        var constraints: [NSLayoutConstraint] = []
        
        let h_emptyView = "H:|-5-[emptyView]-5-|"
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        constraints += NSLayoutConstraint.constraints(withVisualFormat: h_emptyView, options: .alignAllTop, metrics: nil, views: views)
        constraints += [NSLayoutConstraint(item: emptyView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)]
        constraints += [NSLayoutConstraint(item: emptyView, attribute: .top, relatedBy: .equal, toItem: view.layoutMarginsGuide, attribute: .top, multiplier: 1, constant: 50)]
        
        NSLayoutConstraint.activate(constraints)
    }

}

extension HomeViewController: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewDidReceiveAd")
        
    }
    
    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        print("GADMobileAds bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func bannerViewDidRecordImpression(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewDidRecordImpression")
    }

    func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewWillPresentScreen")
    }

    func bannerViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewWillDIsmissScreen")
    }

  
    func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("GADMobileAds bannerViewDidDismissScreen")
    }
}
