//
//  ContentModel.swift
//  demo prebid
//
//  Created by Jan Sebastian on 24/07/23.
//

import Foundation

enum TypeContent {
    case Ads
    case News
}

struct ContentModel {
    let type: TypeContent
}
